import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class EmployeeIn(BaseModel):
    first_name: str
    last_name: str
    email: str
    password: str

class EmployeeOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    shop_id: int

class EmployeeWithPw(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    hashed_password: str
    shop_id: int
    
def sanitize_employees(employees):
    return [
        EmployeeWithPw(
            id=employee[0],
            first_name=employee[1],
            last_name=employee[2],
            email=employee[3],
            hashed_password=employee[4],
            shop_id=employee[5],
        )
        for employee in employees
    ]

class EmployeeQueries:
    def get_by_email(self, email: str) -> EmployeeWithPw:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM employees
                    WHERE email = %s
                    """,
                    [email]
                )
                result = cur.fetchone()
                return sanitize_employees([result])[0]

    def get_by_id(self, id: int) -> EmployeeWithPw:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM employees
                    WHERE id = %s
                    """,
                    [id]
                )
                result = cur.fetchone()
                
                # if used for FE data
                return sanitize_employees([result])[0]


    def create_employee(self, employee: EmployeeIn, hashed_password: str) -> EmployeeWithPw:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    INSERT INTO employees (
                        first_name,
                        last_name,
                        email,
                        hashed_password,
                        shop_id
                    ) VALUES (
                        %s, %s, %s, %s, 1 -- hardcoding shop_id for now
                    )
                    RETURNING *;
                    """,
                    [
                        employee.first_name,
                        employee.last_name,
                        employee.email,
                        hashed_password,
                    ]
                )
                result = cur.fetchone()
                return sanitize_employees([result])[0]
